$(document).ready(function(){
	$('#open-close-menu').on('click', function(){
		$('menu.offcanvas-menu').toggleClass('show-menu');
		return false;
	});
	
	$('#open-close-menu').on('click', function(){
		$('body.offcanvas.push.right').toggleClass('show-canvas');
		return false;
	});
});

$(document).ready(function(){
	var accordion = function() {
		$('.accordion .content').hide();
		$('.accordion .title').click(function(){
			$(this).siblings('.accordion .content').slideUp();
			
			if($(this).next().is(':hidden')) {
				$(this).next().slideDown();
			}
		});
		
		accordion();
	};
});