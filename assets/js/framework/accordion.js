var accordion = function() {
	$('.accordion .content').hide();
	$('.accordion .title').click(function(){
		$(this).siblings('.accordion .content').slideUp();
		
		if($(this).next().is(':hidden')) {
			$(this).next().slideDown();
		}
	});
};

jQuery(function($) {
	accordion();
});